import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  CardText,
  CardLink,
  Col,
  Row,
  Button,
  CardGroup,
  Input,
  Container,
  CustomInput
} from "reactstrap"
import { useEffect, useState, memo, useRef } from "react"
import { Play, Pause } from 'react-feather'
import Nouislider from "nouislider-react"
import "nouislider/distribute/nouislider.css"
import moment from 'moment'

import ReactCrop from "react-image-crop"
import 'react-image-crop/dist/ReactCrop.css'

const VideoPlayer = ({ videoSource, onUpdate }) => {
  const [playing, setPlaying] = useState(false)
  const [muted, setMuted] = useState(false)
  const [videoCurrentTime, setVideoCurrentTime] = useState(0)
  const [videoSize, setVideoSize] = useState({ width: 0, height: 0 })
  const [duration, setDuration] = useState(100)
  
  const videoRef = useRef()
  const [timeStart, setTimeStart] = useState(0)
  const [timeEnd, setTimeEnd] = useState(0)

  const togglePlay = () => setPlaying(!playing)
  
  const formatProgress = () => moment.utc(videoCurrentTime * 1000).format("HH:mm:ss", { trim: false })

  const onSliderUpdate = (render, handle, value, un, percent) => {
    if (!value || value.length < 2) return

    const [timeStart, timeEnd] = value

    setTimeStart(timeStart)
    setTimeEnd(timeEnd)

    onUpdate({ timeStart, timeEnd })
  }

  useEffect(() => {
    setPlaying(false)
    setMuted(false)
    setVideoCurrentTime(0)
    setDuration(100)
    setTimeStart(0)
    setTimeEnd(0)
  }, [videoSource])

  // always loop video at the start time
  useEffect(() => {
    const video = videoRef.current

    if (!video) return

    if (video.currentTime < timeStart) {
      video.currentTime = timeStart
    }
    if (video.currentTime > timeEnd) {
      video.currentTime = timeStart
    }
  }, [timeStart, timeEnd, videoCurrentTime])

  useEffect(() => {
    if (!videoRef.current) return

    let interval

    if (playing) {
      videoRef.current.play()

      interval = setInterval(() => setVideoCurrentTime(parseFloat(videoRef.current.currentTime).toFixed(2)), 500)
    } else {
      videoRef.current.pause()
      clearInterval(interval)
    }

    return () => clearInterval(interval)
  }, [playing])

  const handleVideoLoadedMetadata = ({ currentTarget }) => {
    const { videoWidth, videoHeight, duration } = currentTarget

    setVideoSize({ width: videoWidth, height: videoHeight })
    setDuration(duration)
    setTimeEnd(duration)

    // push data to outside component
    onUpdate({
      timeEnd: duration 
    })
  }

  const [crop, setCrop] = useState({
    unit: '%', 
    width: 30,
    // height: 1008,
    // minWidth: 720,
    aspect: 0.71,
    // width: (512 / 720) * 100,
    // height: (512 / 1080) * 100,
    x: 0,
    y: 0
  })

  useEffect(() => {
    if (!crop || !videoRef.current) return

    console.log({ crop, videoSize, ref: videoRef.current })

    const { clientWidth, clientHeight } = videoRef.current
    const scaleX = videoSize.width / clientWidth
    const scaleY = videoSize.height / clientHeight

    onUpdate({ 
      crop: {
        x: crop.x * scaleX,
        y: crop.y * scaleY,
        width: crop.width * scaleX,
        height: crop.height * scaleY
      } 
    })
  
  }, [crop])

  const videoComponent = (
    <video 
      key={videoSource}
      style={{ display: 'block', maxWidth: '100%' }}
      autoPlay={false}
      ref={videoRef}
      loop
      muted={muted}
      onLoadedMetadata={handleVideoLoadedMetadata}
      onLoadStart={e => {
        // You must inform ReactCrop when your media has loaded.
        e.target.dispatchEvent(new Event('medialoaded', { bubbles: true }))
      }}
    >
      <source src={videoSource}/>
    </video>
  )

  const videoControls = (
    <Container>
      <Row style={{ alignItems: 'center', justifyContent: 'flex-start' }}>
        <Col xs="1">
          <Button.Ripple onClick={togglePlay} className='btn-icon' outline  color='primary' disabled={!videoSource}>
            {!playing ? <Play/> : <Pause/>}
          </Button.Ripple>
        </Col>
        <Col xs="2">
          <CardText style={{ alignSelf: 'center', marginRight: 10 }}>{formatProgress()}</CardText>
        </Col>
        <Col xs="6">
          <Nouislider //style={{ flex: 1 }}
            range={{ min: 0, max: duration }} 
            start={[0, duration]} 
            connect 
            onSlide={onSliderUpdate}/>
        </Col>
        <Col xs="2">
          <CustomInput type="checkbox" id="exampleCustomCheckbox" label="Mute" inline checked={muted} onChange={(event) => setMuted(event.target.checked)}/>
        </Col>
      </Row>
    </Container>
  )

  return (
    <Container style={{ 
      marginTop: 10,
      borderStyle: 'dashed',
      borderWidth: 5,
      borderRadius: 1,
      width: '100%'
    }}>
      <ReactCrop 
        onChange={setCrop}
        crop={crop}
        renderComponent={videoComponent} 
        ruleOfThirds
      />

      {videoControls}
    </Container>
  )
}

const Video = () => {
  const [videoFile, setVideoFile] = useState(null)
  const [videoFileUrl, setVideoFileUrl] = useState(null)
  const [FFmpegOptions, setFFmpegOptions] = useState(null)
  const [timeStart, setTimeStart] = useState(0)
  const [timeEnd, setTimeEnd] = useState(0)
  const [transcoding, setTranscoding] = useState(false)
  const [transcodingProgress, setTranscodingProgress] = useState('')
  const [transcodingAction, setTranscodingAction] = useState('')
  const [crop, setCrop] = useState(null)

  const buildFFmpegString = run => {
    const filename = videoFile.name

    const args = [
      '-i', `${run ? filename : `"${videoFile.name}"`}`,
      '-movflags', 'faststart',
      '-t', (timeEnd - timeStart).toFixed(4)
    ]

    if (timeStart) {
      args.unshift('-ss', timeStart)
    }

    if (crop) {
      let crp = `"crop=${crop.width.toFixed(2)}:${crop.height.toFixed(2)}:${crop.x}:${crop.y}"`
      if (run) crp = crp.replace(/"/g, '')
      args.push('-filter:v', crp)
    }

    args.push('-c:a', 'copy')
    args.push(`${run ? 'output.mp4' : `"edit - ${filename}"`}`)

    return run ? args : args.join(' ')
  }

  useEffect(() => {
    if (!videoFile) return

    setFFmpegOptions(
      `ffmpeg ${buildFFmpegString(false)}`
    )

  }, [videoFile, timeEnd, timeStart, crop])

  const handleVideoFileLoad = (event) => {
    const file = event.target.files[0]

    setVideoFile(file)
    setVideoFileUrl(window.URL.createObjectURL(file))

    //reset
    setFFmpegOptions(null)
    setTimeStart(0)
    setTimeEnd(0)
    setTranscoding(false)
    setTranscodingProgress('')
    setTranscodingAction('')
    setCrop(null)
  }

  const handleVideoPlayerUpdate = ({ timeStart, timeEnd, crop }) => {
    if (timeStart) {
      setTimeStart(parseFloat(timeStart).toFixed(2))
    }

    if (timeEnd) {
      setTimeEnd(parseFloat(timeEnd).toFixed(2))
    }

    if (crop) {
      setCrop(crop)
    }
  }

  const runFFmpeg = async () => {
    try {
      const heap_limit = performance.memory.jsHeapSizeLimit
      if (heap_limit) {
        if (videoFile.size * 2.5 > (heap_limit - performance.memory.usedJSHeapSize)) {
          if (!confirm("The given file is so large, it is likely to crash your browser!\n\nContinue?")) {
            // return
          }
        }
      }
    } catch (e) {}

    setTranscoding(true)

    const fs = require('fs')
    const { createFFmpeg, fetchFile } = require('@ffmpeg/ffmpeg')

    const ffmpeg = createFFmpeg({ 
      log: true,
      progress: ({ ratio }) => {
        console.log(`Transcoding Video: ${(ratio * 100.0).toFixed(2)}%`)
        setTranscodingProgress((ratio * 100.0).toFixed(2))
      }
    })
    
    const cmd = buildFFmpegString(true)
    const filename = videoFile.name
    const outputFilename = 'output.mp4'

    console.log({ cmd: cmd.join(' '), filename })

    setTranscodingAction('Loading ffmpeg-core.js')

    await ffmpeg.load()

    setTranscodingAction('Start transcoding')

    ffmpeg.FS('writeFile', filename, await fetchFile(videoFileUrl))
    await ffmpeg.run(...cmd)
    // await ffmpeg.run('-i', filename, outputFilename)

    setTranscodingAction('Complete transcoding')

    // await fs.promises.writeFile(`./${outputFilename}`, ffmpeg.FS('readFile', outputFilename))

    setTranscoding(false)
    setTranscodingProgress('')

    // download file
    const data = ffmpeg.FS('readFile', outputFilename)
    const a = document.createElement('a')
    const fn = decodeURI(outputFilename)
    a.download = fn
    const blob = new Blob([data.buffer], {type: 'video/mp4'})
    a.href = window.URL.createObjectURL(blob)
    a.textContent = `Click here to download [${fn}]!`

    document.querySelector(".download_links").append(a)
    a.click()
  }

  const renderVideo = () => (
    <Card>
      <CardHeader>
        <CardTitle>Upload Your Video</CardTitle>
      </CardHeader>
      <CardBody>

      <Input type="file" name="file" id="exampleFile" accept=".mkv,video/*" onChange={handleVideoFileLoad}/>

      {videoFile && <VideoPlayer videoSource={videoFileUrl} onUpdate={handleVideoPlayerUpdate}/>}

      </CardBody>
    </Card>
  )

  return (
    <Container>
      <Row>
        <Col xs="8">
      {renderVideo()}
      </Col>
      <Col xs="4">
      <Card>
        <CardBody>
          <CardText style={{ backgroundColor: 'grey', padding: 10, borderRadius: 10, fontFamily: 'monospace', color: 'white' }}>
            {FFmpegOptions}
          </CardText>

          <Button color="primary" size="lg" block onClick={() => runFFmpeg()} disabled={!videoFile || transcoding}>Run FFmpeg in-Browser</Button>

          <CardText style={{ marginTop: 20, fontFamily: 'monospace' }}>
            Running FFmpeg in-browser is unstable, and may crash. It will not work on large files.
            It may also take a while to get started, as it must download a large (26Mb) library to run.
          </CardText>

          {
            transcoding && 
              <CardText style={{ marginTop: 20, fontFamily: 'monospace' }}>
                {transcodingAction}{'\n'}{transcodingProgress}%
              </CardText>
          }
        </CardBody>

        <div className="download_links"></div>

      </Card>
      </Col>
      </Row>
    </Container>
  )
}

export default Video

import { Mail, Home, Video } from 'react-feather'

export default [
  // {
  //   id: 'home',
  //   title: 'Home',
  //   icon: <Home size={20} />,
  //   navLink: '/home'
  // },
  // {
  //   id: 'secondPage',
  //   title: 'Second Page',
  //   icon: <Mail size={20} />,
  //   navLink: '/second-page'
  // },
  {
    id: 'video',
    title: 'Video Transcode',
    icon: <Video size={20} />,
    navLink: '/video',
    meta: {
      contentWidth: 'boxed'
    }
  }
]
